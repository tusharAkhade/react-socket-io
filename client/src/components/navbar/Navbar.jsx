import React, { useState, useEffect } from "react";
import "./navbar.css";
import Message from "../../img/message.svg";
import Settings from "../../img/settings.svg";
import { IoMdNotifications } from "react-icons/io";

const Navbar = ({ socket }) => {
  const [notifications, setNotifications] = useState([]);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    // socket.on => take event from server
    socket.on("getNotification", (data) => {
      setNotifications((prev) => [...prev, data]);
    });
  }, [socket]);

  console.log(notifications);

  const displayNotification = ({ senderName, type }) => {
    let action;
    if (type === 1) {
      action = "liked";
    } else if (type === 2) {
      action = "commented";
    } else if (type === 3) {
      action = "shared";
    }

    return (
      <span className="notification">{`${senderName} ${action} your post`}</span>
    );
  };

  const handleRead = () => {
    setNotifications([]);
    setOpen(false);
  };

  return (
    <div className="navbar">
      <span className="logo">React Socket-IO Demo App</span>
      <div className="icons">
        <div className="icon" onClick={() => setOpen(!open)}>
          <IoMdNotifications className="iconImg" size={20} />
          {notifications.length > 0 && (
            <div className="counter">{notifications.length}</div>
          )}
        </div>
        <div className="icon">
          <img src={Message} className="iconImg" alt="" />
        </div>
        <div className="icon">
          <img src={Settings} className="iconImg" alt="" />
        </div>
      </div>
      {open && (
        <div className="notifications">
          {notifications.map((notification) =>
            displayNotification(notification)
          )}
          <button className="notificationButton" onClick={handleRead}>
            Mark as read
          </button>
        </div>
      )}
    </div>
  );
};

export default Navbar;
